# YAML Tasks

This project defines a YAML format for basic question (single/multiple-choice, direct answer (specific value to answer), and open answers (write text or submit file)).
The format can be used to create an XML file that can be [imported to Moodle](https://docs.moodle.org/310/en/Import_questions).
That is the intended usage.

As you might present the solutions to a test/quiz to students this tool also supports the export to documents.
That is done through [Pandoc](https://pandoc.org/) and hence supports a bunch of format.
This feature is not polished, to achieve a workable file you will probably have to modify the Pandoc flags.
Our main use case for this would be to generate a `.pptx` presentation and polish the look manually, as the program can't realistically assume all the corner cases what could look good.

Further details on this below.

# Format Description

## Structure

Questions (see next section) are stored in `.yaml` format.
Multiple questions in one file (separated with `---`) are considered one task.
All files (tasks) in a folder are considered one exercise.
If a whole folder (i.e. one exercise) is parsed, the tasks are numbered in according to their position in the sorted list of files (e.g.: `a.yaml` is task 1, `b.yaml` is task 2).

Example document tree:
```
exercises
 |
 +----01
 |
 \----02
      |
      +----task01.yaml
      |
      \----task02.yaml
```

## Elements

Every element supports four properties: `type`, `title`, `text`, and `files`.
All except `files` are required (though `text` may be empty).

- `type` determines the type of the element, allowed values are:
  - `description`,
  - `single-choice`,
  - `multiple-choice`,
  - `direct-answer`, and
  - `open-answer`.
  These are described in further detail below.
- `title` set the title of an element. The title will be visible to both the students and in the question catalogue in Moodle.
  The visibility for students can be turned off through the format string that defines it (see CLI options `task-title-format` and `question-title-format`).
- `text` is the description of the task or question.
- `files` is optional and is a list of files that will be appended to the `text` element.

  A file's path is relative to the `.yaml` you are writing.

  So if you have something like:
  ```yaml
  text: |-
    Lorem ipsum

  files:
    - a02.yaml
  ```

  You will get something like:
  ```html
  <text><![CDATA[
  <p>Lorem ipsum</p>
  <p> </p>
  <p><a title="a02.yaml" href="@@PLUGINFILE@@/a02.yaml" target="_blank">a02.yaml</a></p>
  ]]></text>

  <file name="a02.yaml" path="/" encoding="base64">...
  ```

### Text Processing

Every text that is displayed in Moodle (namely `text` and the `answers` object for single/multiple-choice questions) are considered Markdown.
Direct HTMl is supported and everything that is between `\(` and `\)` is left as-is. Moodle displays this using [MathJax](https://www.mathjax.org/).


If you want to add your own text processing, have a look at `yaml_tasks/yaml_parser/utils.py`.

### Descriptions

The `title` of a description will be given an header tag, so with an empty `text` it can simply serve as a header.
It is probably an good idea to have a description at the beginning of every task file, as every description element starts a new task.

Required:
- `type: description`
- `title` string
- `text` string (may be empty)

Example:
```yaml
title: Test
type: description
text: |-
  Lorem ipsum <img src="img.jpg" alt="text"/>

files:
  - a02.yaml
```

### Question

This format currently supports four types of questions.
All **require** the keys: `title`, `type`, `points`, and `text`.

### Question Types
#### `single-choice` and `multiple-choice`

For single and multiple choice answers.

The possible entries for this type are:
- `answers` (required, list of dicts) where the dict required `true` or `false` as key and optionally `fraction`, which overwrites the computed percentage that is given for the specific answer.
  ```yaml
  answers:
    - correct: Answer1
    - correct: Answer2
      fraction: +50
    - incorrect: Answer3
    - incorrect: Answer4
    - incorrect: Answer5
  ```
- `variance` (not required (only for multiple choice), number in percent)
  in case this is not set all incorrect answers are weighted, so that selecting ALL answers results in 0pt for the student (`relative_weight = 100%/(#incorrect)`).\
  If the `variance` is set, the `relative_weight` is adjusted by the given percentage.

  Example (percentage in braces is added/subtracted from the points if the answer is selected by the student):
  ```
  variance: None              variance: 10              variance: -5
  correct:                    correct:                  correct:
    - Answer 1 (+50%)           - Answer 1 (+50%)         - Answer 1 (+50%)
    - Answer 2 (+50%)           - Answer 2 (+50%)         - Answer 2 (+50%)
  incorrect:                  incorrect:                incorrect:
    - Answer 3 (-25%)           - Answer 3 (-15%)         - Answer 3 (-30%)
    - Answer 4 (-25%)           - Answer 4 (-15%)         - Answer 4 (-30%)
    - Answer 5 (-25%)           - Answer 5 (-15%)         - Answer 5 (-30%)
    - Answer 6 (-25%)           - Answer 6 (-15%)         - Answer 6 (-30%)
  ```
  This computation is not affected by the `fraction` value of specific answers.
- `shuffle_answers` (not required, boolean, default true, default false if `--dont-shuffle-answers` is set on CLI)
  determines whether the answers will be shuffled by Moodle.
  If `false` the same order as in the YAML will be displayed.

#### `direct-answer`

For answers that require a direct match (alphabetical).
The possible entries for this type are:
- `answer` (required, any 'simple' type like str, int, float, ... and a list of those) the value
  - e.g.:
    ```
    answer:
      - 42
      - \x2A
    ```
    ```
    answer: '42'
    ```
- `usecase` (not required, bool, default: True) whether to check for equal casing of the answer, or not.

#### `open-answer`

For free text or file-based answers.
The possible entries for this type are:
- `file-submission` (not required, dict) default: `allow=True`
  - `allow` or `require` (exactly one of both is required, bool or int) determines if files are allowed or required.
    `allow=True` is equal to `allow=-1` and implies, that the number of uploaded files is unlimited, non are required.
    `allow=False` is equal to `allow=0` and implies, that no files are allowed to be uploaded.
    `allow=1`, `allow=2`, `allow=3`: number of allowed files.

    `require=1`, `require=2`, `require=3`: number of files that are required to be uploaded.
  <!-- - `types` (not required, list[string]) all allowed filetypes,
    if none given, but `allow=True` all filetypes are allowed
    TODO: implement-->
- `require-text-submission` (not required, bool) determines if the student has to write something in the box, default: False
- `text-submission-template` (not required, str) text that is displayed in the answer textbox for the student to build upon, default: empty.
  If `file-submission` is required and text submission is not, then this assumes that only files should be uploaded (moodle does not allow the text field to be disabled).
  For this case `text-submission-template` is set to `Upload file below` per default.

## Example

`task01.yaml`:
```yaml
---
title: Test 2
type: description
text: |-
  Lorem ipsum <img src="img.jpg" alt="text"/>
---

title: Q1
type: single-choice
points: 10
text: |-
  Lorem ipsum

answers:
  - correct: Answer 1
  - incorrect: Answer 3
  - incorrect: Answer 4
  - incorrect: Answer 5
  - incorrect: Answer 6
  - incorrect: Answer 7

---

title: Q2
type: multiple-choice
points: 10
variance: -5
text: |-
  Lorem ipsum

answers:
    - correct: Answer1
    - correct: Answer2
      fraction: +50
    - incorrect: Answer3
    - incorrect: Answer4
    - incorrect: |-
        multi line
        answer

---

title: Q3
type: direct-answer
points: 5
text: |-
  Lorem ipsum
files:
  - some_file.pdf

answer: 42
```

`task02.yaml`:
```yaml
---
title: Test 2
type: description
text: |-
  Lorem ipsum

files:
  - a02.yaml
---

title: Q4
type: open-answer
points: 12
text: |-
  Lorem ipsum

file-submission:
  require: 1
require-text-submission: False
```

# Usage

```
[-h] [--category-format CATEGORY_FORMAT] [--task-title-format TASK_TITLE_FORMAT] [--question-title-format QUESTION_TITLE_FORMAT] [--output OUTPUT] [--use-pandoc PANDOC_FORMAT] [--pandoc-flags PANDOC_FLAGS] FILE|DIR [FILE|DIR ...]
```

## Arguments

- `FILE|DIR`
  the paths to the YAML files or directory of YAML files. If a directory is given all `.yaml` files in the dir are parsed in sorted order. The overall order of all files (after dirs are 'expanded') determines the order of tasks in the resulting file.
  That means given the parameters` /path/to/03.yaml /path/to/dir` (where `/path/to/dir` contains `01.yaml` and `02.yaml`) results in the order `03 01 02`. Silently drops non-existent files/dirs.
- `-h, --help`
  show this help message and exit
- `--category-format CATEGORY_FORMAT, -c CATEGORY_FORMAT`
  Format string to be used as the category for each question. When importing into Moodle via the XML file this determines for each question where it is stored.Supports the variables:
  - `task_title`: the title of the latest description that was parsed in this file.
  - `file_name`: the name of the file currently parsed.
  - `task_nr`: the number of the currently parsed task.
  - `entry_nr`: the number of the currently parsed question/description (resets with new `task_nr`).
- `--task-title-format TASK_TITLE_FORMAT, -t TASK_TITLE_FORMAT`
  Format string, that determines the style of the task header (title of a description element). This will be directly rendered by the Moodle webpage. Supports the variables:
  - `task_title`: the title of the task currently being parsed (the title of the latest description).
  - `task_nr`: the number of the currently parsed task.
- `--question-title-format QUESTION_TITLE_FORMAT, -q QUESTION_TITLE_FORMAT`
  Format string, that determines the style of the question header(title of a question element). This will be directly rendered by the Moodle webpage. Supports the variables:
  - `task_title`: the title of the task currently being parsed (the title of the latest description).
  - `task_nr`: the number of the currently parsed task.
  - `question_title`: the title of the current question.
  - `question_nr`: the number of the question in this task.
  - `question_nr_alph`: `question_nr` converted to an alphabetic format (1:a, 2:b, ..., 27:aa, 28:ab, ...).
- `--output OUTPUT, -o OUTPUT`
  location to output the resulting XML file to. Defaults to {CWD}/output.xml
- `--use-pandoc PANDOC_FORMAT, -p PANDOC_FORMAT`
  Use pandoc (needs to be installed) to create a document instead of the XML file. You can use any of the formats that pandoc can transfer to, see https://pandoc.org or `pandoc --list-output-formats`. If the format is `html`, the internally produced `html` file will be outputted. This is great for debugging, or if something needs to be modified manually so the generated file look good (e.g. adding `<hr/>` starts a new slide in presentations).ATTENTION: this generation is very fragile, depending on what format you covert to.
- `--pandoc-flags PANDOC_FLAGS, -f PANDOC_FLAGS`
  Does nothing if `-p` is not set. You can add arbitrary pandoc flags as a string here. If it will work, is kinda up to what you add in here.

# Details (Implementation and Advanced Usage)

Some points about the generation of documents:

- You should roughly understand how pandoc works, otherwise you might get frustrated...
- The tool internally creates an HTML file that is piped into pandoc.
  If something looks weird or you want to change something: have a look at the HTML by using `--use-pandoc html`
- The library that calls pandoc does not report problems back...
  Things I've noticed with the current setup:
  - The checkmarks, etc that are added after correct answers are not rendered on my machine if using LaTeX Beamer (font is missing the symbols). To solve it one can change the values in `PandocBuilder`'s init, or programmatically change the dict property that defines it.
    When using `beamer` you definitely need `--pandoc-flags "\"--pdf-engine=xelatex\""`
  - When using `pptx` images sometimes get missing, no idea why though.

  When encountering weird stuff: generate the HTML and throw it at pandoc directly.
- Interesting flags for pandoc:
  - `--slide-level` sets the header level that are considered titles, headers, ... in slides.
  - If you miss functionality that is not handled correctly by pandoc: there probably is a filter for it.

# TODO

- [ ] Add options for feedback / correct answer
  - Should be possible to add feedback for each answer in multi-choice, direct-answer, ...
- [ ] Add option of fractions on direct-answer (list of answers with different percentages)
