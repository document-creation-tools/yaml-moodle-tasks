import json
import logging
import pprint

import colorlog


class DictLogger(logging.getLoggerClass()):
    """A logger that can log dictionaries."""

    LOG_COUNTS = {}

    def _log_fancy(self, level, msg, exc_info, extra, stack_info, stacklevel):
        """Low-level. Log a dictionary with the given level. Does not support args.
        Formats the dictionary as JSON if possible, otherwise uses pprint.pformat."""
        dump = None
        if not isinstance(msg, str):
            try:
                dump = json.dumps(msg, indent=4)
            except TypeError:
                dump = pprint.pformat(msg)
        if not dump:
            dump = str(msg)

        for line in dump.splitlines():
            super()._log(level, line, (), exc_info, extra, stack_info, stacklevel)

    def _log(self, level, msg, args, exc_info=None, extra=None, stack_info=False, stacklevel=1):
        if level not in DictLogger.LOG_COUNTS:
            DictLogger.LOG_COUNTS[level] = 0
        DictLogger.LOG_COUNTS[level] += 1
        if args:
            super()._log(level, msg, args, exc_info, extra, stack_info, stacklevel)
            return
        self._log_fancy(level, msg, exc_info, extra, stack_info, stacklevel)


logging.setLoggerClass(DictLogger)

handler = logging.StreamHandler()
handler.setFormatter(
    colorlog.ColoredFormatter(
        "%(log_color)s%(levelname)7s | %(message)s",
        log_colors={
            "DEBUG": "thin_white",
            "INFO": "white",
            "WARNING": "yellow",
            "ERROR": "red",
            "CRITICAL": "bold_red",
        },
    )
)

logger = logging.getLogger("yaml_tasks")
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)
