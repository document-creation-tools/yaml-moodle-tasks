"""Provides standalone functions used in this package."""

import bisect
import os

from marko import Markdown, MarkoExtension
from marko.inline import InlineElement


class InlineLatexMath(InlineElement):
    """custom inline element for LaTeX math"""

    pattern = r"(\\\(.*?\\\))"
    priority = 10

    def __init__(self, match):
        self.target = match.group(0)


class LatexMathRendererMixin(object):
    """custom renderer for LaTeX math that keeps the LaTeX math verbatim"""

    def render_inline_latex_math(self, element):
        return element.target


mark = Markdown(
    extensions=[
        "codehilite",
        MarkoExtension(elements=[InlineLatexMath], renderer_mixins=[LatexMathRendererMixin]),
    ]
)


def preprocess(markdown: str, no_outer_p: bool = False) -> str:
    """Uses a number of functions to preprocess text from the YAML file."""

    html = mark.convert(markdown)

    if no_outer_p and html.startswith("<p>") and html.endswith("</p>\n"):
        html = html[3:-5]

    return html


def get_styles() -> str:
    base = (
        "background-color: rgba(0, 0, 0, 0.06); "
        "border-radius: 6px; "
        "padding: 0.2em 0.4em; "
        "color: black;"
    )
    styles = [
        f"code {{ {base} }}",
        # from: https://github.com/richleland/pygments-css/blob/master/bw.css
        f".highlight {{ {base} }}",
        # ".highlight .hll { background-color: #ffffcc }",
        ".highlight .c { font-style: italic }",  # Comment
        ".highlight .err { border: 1px solid #FF0000 }",  # Error
        ".highlight .k { font-weight: bold }",  # Keyword
        ".highlight .ch { font-style: italic }",  # Comment.Hashbang
        ".highlight .cm { font-style: italic }",  # Comment.Multiline
        ".highlight .cpf { font-style: italic }",  # Comment.PreprocFile
        ".highlight .c1 { font-style: italic }",  # Comment.Single
        ".highlight .cs { font-style: italic }",  # Comment.Special
        ".highlight .ge { font-style: italic }",  # Generic.Emph
        ".highlight .gh { font-weight: bold }",  # Generic.Heading
        ".highlight .gp { font-weight: bold }",  # Generic.Prompt
        ".highlight .gs { font-weight: bold }",  # Generic.Strong
        ".highlight .gu { font-weight: bold }",  # Generic.Subheading
        ".highlight .kc { font-weight: bold }",  # Keyword.Constant
        ".highlight .kd { font-weight: bold }",  # Keyword.Declaration
        ".highlight .kn { font-weight: bold }",  # Keyword.Namespace
        ".highlight .kr { font-weight: bold }",  # Keyword.Reserved
        ".highlight .s { font-style: italic }",  # Literal.String
        ".highlight .nc { font-weight: bold }",  # Name.Class
        ".highlight .ni { font-weight: bold }",  # Name.Entity
        ".highlight .ne { font-weight: bold }",  # Name.Exception
        ".highlight .nn { font-weight: bold }",  # Name.Namespace
        ".highlight .nt { font-weight: bold }",  # Name.Tag
        ".highlight .ow { font-weight: bold }",  # Operator.Word
        ".highlight .sa { font-style: italic }",  # Literal.String.Affix
        ".highlight .sb { font-style: italic }",  # Literal.String.Backtick
        ".highlight .sc { font-style: italic }",  # Literal.String.Char
        ".highlight .dl { font-style: italic }",  # Literal.String.Delimiter
        ".highlight .sd { font-style: italic }",  # Literal.String.Doc
        ".highlight .s2 { font-style: italic }",  # Literal.String.Double
        ".highlight .se { font-weight: bold; font-style: italic }",  # Literal.String.Escape
        ".highlight .sh { font-style: italic }",  # Literal.String.Heredoc
        ".highlight .si { font-weight: bold; font-style: italic }",  # Literal.String.Interpol
        ".highlight .sx { font-style: italic }",  # Literal.String.Other
        ".highlight .sr { font-style: italic }",  # Literal.String.Regex
        ".highlight .s1 { font-style: italic }",  # Literal.String.Single
        ".highlight .ss { font-style: italic }",  # Literal.String.Symbol
    ]
    return f"<style>{ ' '.join(styles) }</style>"


FRACTIONS = [
    -100.0,
    -90.0,
    -83.33333,
    -80.0,
    -75.0,
    -70.0,
    -66.66667,
    -60.0,
    -50.0,
    -40.0,
    -33.33333,
    -30.0,
    -25.0,
    -20.0,
    -16.66667,
    -14.28571,
    -12.5,
    -11.11111,
    -10.0,
    -5.0,
    0.0,
    5.0,
    10.0,
    11.11111,
    12.5,
    14.28571,
    16.66667,
    20.0,
    25.0,
    30.0,
    33.33333,
    40.0,
    50.0,
    60.0,
    66.66667,
    70.0,
    75.0,
    80.0,
    83.33333,
    90.0,
    100.0,
]


def closest_allowed_fraction(_frac):
    """Returns the fraction in FRACTIONS closest to the given number."""
    _rounded = round(_frac, 5)
    if _rounded in FRACTIONS:
        return _rounded
    # gets the index where _frac would be inserted into FRACTIONS
    i = bisect.bisect_left(FRACTIONS, _frac)
    if i == 0:
        return FRACTIONS[0]
    if i == len(FRACTIONS):
        return FRACTIONS[-1]
    left = FRACTIONS[i - 1]
    right = FRACTIONS[i]
    if abs(left - _frac) < abs(right - _frac):
        return left
    else:
        return right


# https://stackoverflow.com/a/23862195
def get_alphabetical_repr(number: int) -> str:
    """Gets an alphabetical representation of a number.
    1 -> a, 2 -> b, 27 -> aa, 28 -> ab, ...
    """
    string = ""
    while number > 0:
        number, remainder = divmod(number - 1, 26)
        string = chr(97 + remainder) + string
    return string


def get_file_name(path: str) -> str:
    """Returns a file's name without extension(s)."""
    return os.path.basename(path).split(".")[0]


def get_list(item):
    """Ensures, that a given item is a list."""
    if isinstance(item, list):
        return item
    else:
        return [str(item)]
