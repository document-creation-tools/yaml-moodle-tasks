"""Validates entries from the YAML file to be in the correct format/schema."""

from cerberus import Validator


class ValidationError(Exception):
    """Custom exception that is thrown if the validation failed."""


GENERAL_SCHEMA = {
    "type": {
        "type": "string",
        "required": True,
        "allowed": [
            "description",
            "single-choice",
            "multiple-choice",
            "direct-answer",
            "open-answer",
        ],
    },
    "title": {
        "type": "string",
        "required": True,
        "empty": False,
    },
    "text": {
        "type": "string",
        "required": True,
        "empty": True,
    },
    "files": {
        "type": "list",
        "required": False,
        "schema": {
            "type": "string",
            "empty": False,
        },
    },
}

QUESTION_SCHEMA = {
    "points": {
        "type": "integer",
        "required": True,
        "empty": False,
    },
}

TYPE_SPECIFIC_SCHEMAS = {
    "single-choice": {
        "answers": {
            "type": "list",
            "required": True,
            "schema": {
                "type": "dict",
                "schema": {
                    "correct": {
                        "type": "string",
                        "excludes": "incorrect",
                        "required": True,
                    },
                    "incorrect": {
                        "type": "string",
                        "excludes": "correct",
                        "required": True,
                    },
                },
            },
        },
        "shuffle_answers": {
            "type": "boolean",
            "required": False,
        },
    },
    "multiple-choice": {
        "answers": {
            "type": "list",
            "required": True,
            "schema": {
                "type": "dict",
                "schema": {
                    "correct": {
                        "type": "string",
                        "excludes": "incorrect",
                        "required": True,
                    },
                    "incorrect": {
                        "type": "string",
                        "excludes": "correct",
                        "required": True,
                    },
                    "fraction": {
                        "type": "number",
                        "required": False,
                        "min": -100,
                        "max": 100,
                    },
                },
            },
        },
        "variance": {
            "type": "number",
            "required": False,
            "min": -100,
            "max": 100,
        },
        "shuffle_answers": {
            "type": "boolean",
            "required": False,
        },
    },
    "direct-answer": {
        "answer": {
            "required": True,
            "oneof": [
                {
                    "anyof_type": [
                        "boolean",
                        "binary",
                        "number",
                        "string",
                    ],
                },
                {
                    "type": "list",
                    "schema": {
                        "anyof_type": [
                            "boolean",
                            "binary",
                            "number",
                            "string",
                        ],
                    },
                },
            ],
        },
        "usecase": {
            "type": "boolean",
            "required": False,
        },
    },
    "open-answer": {
        "file-submission": {
            "type": "dict",
            "required": False,
            "schema": {
                "allow": {
                    "oneof": [
                        {
                            "type": "boolean",
                        },
                        {
                            "type": "integer",
                            "min": -1,
                            "max": 3,
                        },
                    ],
                    "excludes": "require",
                    "required": True,
                },
                "require": {
                    "type": "integer",
                    "min": 1,
                    "max": 3,
                    "excludes": "allow",
                    "required": True,
                },
                "types": {
                    "type": "list",
                    "required": False,
                    "schema": {
                        "type": "string",
                    },
                },
            },
        },
        "require-text-submission": {
            "type": "boolean",
            "required": False,
        },
    },
}


def validate(values):
    """Validates entries from the YAML file to be in the correct format/schema."""

    def _validate(values, schema):
        _v = Validator(schema)
        if not _v.validate(values):
            raise ValidationError(_v.errors)

    if values["type"] == "description":
        _validate(values, GENERAL_SCHEMA)
    else:
        _validate(
            values,
            {
                **GENERAL_SCHEMA,
                **QUESTION_SCHEMA,
                **TYPE_SPECIFIC_SCHEMAS[values["type"]],
            },
        )
