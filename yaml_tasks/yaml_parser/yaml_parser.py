"""Parses given YAML task files and stores the results in an internal DB.
"""

import json
import logging
import os

from ruamel.yaml import YAML
from ruamel.yaml.constructor import DuplicateKeyError
from ruamel.yaml.scanner import ScannerError

from .utils import (
    closest_allowed_fraction,
    get_alphabetical_repr,
    get_file_name,
    get_list,
    get_styles,
    preprocess,
)
from .validator import ValidationError, validate

logger = logging.getLogger("yaml_tasks")


class EntryDB:
    """Stores the entries to be used by a builder."""

    def __init__(self):
        self._tasks = {}

    def add_entry(
        self,
        task_nr: int,
        entry_nr: int,
        category: str,
        entry_type: str,
        properties: dict,
    ) -> None:
        """Adds an entry to the internal data structure."""

        logger.info("Add %s in %s:", entry_type, category)
        logger.info(properties)

        _entry = {
            "category": category,
            "type": entry_type,
            "properties": properties,
        }

        if task_nr not in self._tasks:
            self._tasks[task_nr] = {}

        self._tasks[task_nr][entry_nr] = _entry

    def get_entries(self):
        """Returns all entries as a dict."""
        return self._tasks


class YamlParser:
    """Add the questions from a YAML task file to an internal state.

    Args:
        category_format:
            Format string to be used as the category for each question. When importing into Moodle
            via the XML file this determines for each question where it is stored.
            Supports the variables:
            - task_title: the title of the latest description that was parsed in this file
            - file_name: the name of the file currently parsed
            - task_nr: the number of the currently parsed task
            - entry_nr: the number of the currently parsed question/description
              (resets with new task_nr)
        task_title_format:
            Format string, that determines the style of the task header
            (title of a description element). This will be directly rendered by the Moodle webpage.
            Supports the variables:
            - task_title: the title of the task currently being parsed
              (the title of the latest description)
            - task_nr: the number of the currently parsed task
        question_title_format:
            Format string, that determines the style of the question header
            (title of a question element). This will be directly rendered by the Moodle webpage.
            Supports the variables:
            - task_title: the title of the task currently being parsed
              (the title of the latest description)
            - task_nr: the number of the currently parsed task
            - question_title: the title of the current question
            - question_nr: the number of the question in this task
            - question_nr_alph: question_nr converted to an alphabetic format
              (1:a, 2:b, ..., 27:aa, 28:ab, ...)
        shuffle_answers:
            Whether to shuffle the answers in a single/multiple choice question.
            (This is the default that can be overwritten in the YAML file.)
    """

    def __init__(
        self,
        category_format: str = "$module$/top/{task_title}",
        task_title_format: str = "<h1>Task {task_nr}: {task_title}</h1>",
        question_title_format: str = "<h2>{task_nr}{question_nr_alph}) {question_title}</h2>",
        shuffle_answers: bool = True,
    ):
        self.category_format = category_format
        self.task_title_format = task_title_format
        self.question_title_format = question_title_format
        self.shuffle_answers = shuffle_answers
        self._category = ""
        self._current_yaml_file_path = ""
        self._yaml = YAML(typ="safe")
        self._task_nr = 0
        self._current_task_name = ""
        self._entry_nr = 0
        self._db = EntryDB()

    def parse_file(self, file_path: str):
        """Parses the YAML file at the specified path into the internal format."""
        self._current_yaml_file_path = file_path
        self._update_category()
        try:
            with open(self._current_yaml_file_path, "r", encoding="utf-8") as file:
                for entry_nr, entry in enumerate(self._yaml.load_all(file.read())):
                    if entry is None:
                        continue
                    try:
                        validate(entry)
                        self._process_entry(entry)
                    except ValidationError as err:
                        logger.error(
                            "Error validating YAML entry %s (title: %s) in %s against the schema\n"
                            "The validator reports: %s\n",
                            entry_nr,
                            entry.get("title", "<no title found>"),
                            self._current_yaml_file_path,
                            err,
                        )
        except FileNotFoundError:
            logger.error("Could not open file %s", self._current_yaml_file_path)
        except DuplicateKeyError:
            logger.error(
                "Error parsing YAML: duplicate keys.\n"
                "File: %s\n"
                "Did you maybe forget a dividing --- between two questions?",
                self._current_yaml_file_path,
            )
        except ScannerError as err:
            logger.error(
                "Error parsing YAML: %s\nFile: %s\nLine: %s\nColumn: %s\n",
                err.problem,
                self._current_yaml_file_path,
                err.problem_mark.line,
                err.problem_mark.column,
            )

    def get_parsed_data(self):
        """Returns the parsed data as a dict."""
        return self._db.get_entries()

    def _process_entry(self, entry):
        """Processes any element that has been verified to adhere to the specified format."""
        if entry["type"] == "description":
            self._task_nr += 1
            self._entry_nr = 0
        else:
            self._entry_nr += 1
        self._update_category()

        def _process_description():
            self._current_task_name = entry["title"]
            return (
                "description",
                {
                    # this title is only visible in the question collection in Moodle
                    # it gets numbered, so it is sorted correctly
                    "title": f"{self._task_nr:02} {self._current_task_name}",
                    "description": self.task_title_format.format(
                        task_title=self._current_task_name,
                        task_nr=self._task_nr,
                    )
                    + "\n"
                    + get_styles()
                    + "\n"
                    + preprocess(entry["text"]),
                    "file_paths": self._get_file_paths(entry.get("files")),
                },
            )

        def __new_question_entry(old_entry):
            return {
                # this title is only visible in the question collection in Moodle
                # it gets numbered, so it is sorted correctly
                "title": f"{self._task_nr:02}.{self._entry_nr:02} {old_entry['title']}",
                "question": self.question_title_format.format(
                    task_title=self._current_task_name,
                    task_nr=self._task_nr,
                    question_title=old_entry["title"],
                    question_nr=self._entry_nr,
                    question_nr_alph=get_alphabetical_repr(self._entry_nr),
                )
                + "\n"
                + preprocess(entry["text"]),
                "points": old_entry["points"],
                "file_paths": self._get_file_paths(entry.get("files")),
            }

        def _process_choice():
            new_entry = __new_question_entry(entry)
            if "shuffle_answers" in entry:
                new_entry["shuffle_answers"] = entry["shuffle_answers"]
            else:
                new_entry["shuffle_answers"] = self.shuffle_answers

            _correct = 0
            _incorrect = 0
            for answer in entry["answers"]:
                if "correct" in answer:
                    _correct += 1
                elif "incorrect" in answer:
                    _incorrect += 1

            if entry["type"] == "single-choice":
                new_entry["single_choice"] = True
                _fraction_correct = 100
                _fraction_incorrect = 0
            else:
                new_entry["single_choice"] = False
                if "variance" in entry:
                    _variance = entry["variance"] / 100
                else:
                    _variance = 0
                if _correct > 0:
                    _fraction_correct = 100 / _correct
                if _incorrect > 0:
                    _fraction_incorrect = -100 / _incorrect + _variance

            new_entry["answers"] = []
            for answer in entry["answers"]:
                if "correct" in answer:
                    _frac = answer.get("fraction", _fraction_correct)
                    _text = answer["correct"]
                elif "incorrect" in answer:
                    _frac = answer.get("fraction", _fraction_incorrect)
                    _text = answer["incorrect"]

                new_entry["answers"].append(
                    {
                        "text": preprocess(str(_text), no_outer_p=True),
                        "fraction": closest_allowed_fraction(_frac),
                    }
                )

            return ("multichoice", new_entry)

        def _process_direct_answer():
            new_entry = __new_question_entry(entry)
            new_entry["answers"] = []
            for answer in get_list(entry["answer"]):
                new_entry["answers"].append(
                    {
                        "text": answer,
                        "fraction": 100,
                    }
                )
            new_entry["usecase"] = entry.get("usecase", True)

            return ("shortanswer", new_entry)

        def _process_open_answer():
            new_entry = __new_question_entry(entry)

            new_entry["responseformat"] = "editor"
            new_entry["responsetemplate"] = entry.get("text-submission-template", "")
            new_entry["responsefieldlines"] = 15

            if "require-text-submission" in entry:
                new_entry["responserequired"] = entry["require-text-submission"]
            else:
                new_entry["responserequired"] = False

            if "file-submission" not in entry:
                new_entry["attachments"] = -1
                new_entry["attachmentsrequired"] = 0
            else:
                if "allow" in entry["file-submission"]:
                    _allow = entry["file-submission"]["allow"]
                    if isinstance(_allow, bool):
                        if _allow:
                            new_entry["attachments"] = -1
                        else:
                            new_entry["attachments"] = 0
                    else:
                        new_entry["attachments"] = _allow
                    new_entry["attachmentsrequired"] = 0
                if "require" in entry["file-submission"]:
                    new_entry["attachments"] = entry["file-submission"]["require"]
                    new_entry["attachmentsrequired"] = entry["file-submission"]["require"]
                    if not new_entry["responserequired"]:
                        # assume no text shall be given in this case:
                        new_entry["responseformat"] = "plain"
                        if new_entry["responsetemplate"] == "":
                            new_entry["responsetemplate"] = "Upload file below"
                        new_entry["responsefieldlines"] = 5
                # TODO: add type list

            return ("essay", new_entry)

        _function_mapping = {
            "description": _process_description,
            "single-choice": _process_choice,
            "multiple-choice": _process_choice,
            "direct-answer": _process_direct_answer,
            "open-answer": _process_open_answer,
        }

        _processed_entry = _function_mapping[entry["type"]]()

        self._db.add_entry(
            self._task_nr,
            self._entry_nr,
            self._category,
            _processed_entry[0],
            _processed_entry[1],
        )

    def _update_category(self):
        """Updates the category format string with the current values."""
        self._category = self.category_format.format(
            task_title=self._current_task_name,
            file_name=get_file_name(self._current_yaml_file_path),
            task_nr=self._task_nr,
            entry_nr=self._entry_nr,
        )

    def _get_file_paths(self, _path_list=None):
        """Prefix the file list with the location of the YAML file.

        This enables relative file positions from the YAML file, to images, etc.
        """
        _path_base = os.path.dirname(os.path.abspath(self._current_yaml_file_path))
        if not _path_list:
            return [_path_base]
        return [_path_base] + _path_list
