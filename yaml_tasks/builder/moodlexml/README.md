# Moodle XML Writer

Python interface to programmatically write MoodleXML files.
This project does not assume any structure of the uploaded tasks.

- `writer.py` is a low-level API which actually creates the XML string.
  It mainly takes the arguments and wraps them in XML strings and does not process them, but it does inline images and files that are referenced from a HTML text.
  Hence, it is not really nice to work with.
- `builder.py` is a wrapper around `writer.py` and way nicer to actually program with.
  1. Create a `MoodleXmlBuilder` instance:
      ```python
      builder = MoodleXmlBuilder()
      ```
  2. Add tasks. Currently supported are: description, essay, multichoice, numerical, shortanswer.
      The purpose of each one can be looked up [here](https://docs.moodle.org/30/en/Questions).
      Not every parameter Moodle supports is implemented, but most are.
      To add tasks or descriptions call `builder.add_entry(type, category, arguments)`, where
      - `type` is the type of the entry
      - `category` is the category where the entry should be uploaded to Moodle, this could be, for example, `$course$/top/exercises/01`.
      - `arguments` is a dict, where the keys are the argument names for the entry and the value is the corresponding value.
      For example:
      ```python
      builder.add_entry('description', '$course$/top/exercises/01', {
        'title': 'Task 01: Lorem Ipsum',
        'description': """\
          <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
          <img src="img.jpg" alt="Test Image">
          <ol>
            <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
            <li>Aliquam tincidunt mauris eu risus.</li>
            <li>Vestibulum auctor dapibus neque.</li>
          </ol>"""
        'file_paths': ['os.getcwd()', 'appendix.txt'],  # the first entry in the file_paths array is always a prefix-path for all local file or image paths, if it is not needed simply use an empty string
      })
      ```
      For simplicity you could also use:
      ```python
      builder.add_description( ...
      ```
      instead of
      ```python
      builder.add_entry('description', ...
      ```
  3. After all entries are added simply access the XML string via `builder.xml`.

## Additional Infos

- [How to upload questions to Moodle](https://docs.moodle.org/30/en/Import_questions)
