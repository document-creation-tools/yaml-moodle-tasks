"""Types defined for the MoodleXML project."""
from typing import Literal, NewType, TypedDict, Union

XmlString = NewType("XmlString", str)
HtmlString = NewType("HtmlString", str)
AllowedFraction = Literal[
    "-100.0",
    "-90.0",
    "-83.33333",
    "-80.0",
    "-75.0",
    "-70.0",
    "-66.66667",
    "-60.0",
    "-50.0",
    "-40.0",
    "-33.33333",
    "-30.0",
    "-25.0",
    "-20.0",
    "-16.66667",
    "-14.28571",
    "-12.5",
    "-11.11111",
    "-10.0",
    "-5.0",
    "0.0",
    "5.0",
    "10.0",
    "11.11111",
    "12.5",
    "14.28571",
    "16.66667",
    "20.0",
    "25.0",
    "30.0",
    "33.33333",
    "40.0",
    "50.0",
    "60.0",
    "66.66667",
    "70.0",
    "75.0",
    "80.0",
    "83.33333",
    "90.0",
    "100.0",
]
Answer = TypedDict(
    "Answer",
    {
        "text": str,
        "text_is_markdown": bool,
        "fraction": AllowedFraction,
        "feedback": HtmlString,
        "tolerance": Union[int, float],
    },
    total=False,
)
Feedback = Literal["general", "correct", "partiallycorrect", "incorrect"]
ResponseFormat = Literal["editor", "editorfilepicker", "plain", "monospaced"]
ResponseFieldHeight = Literal[5, 10, 15, 20, 25, 30, 35, 40]
