"""Builder for MoodleXML files, uses writer for the actual XML."""
import io
import logging
from functools import partial
from inspect import signature
from textwrap import dedent, indent

from docstring_parser import parse

from .. import Builder
from .writer import (
    create_xml_category,
    create_xml_description,
    create_xml_essay,
    create_xml_multichoice,
    create_xml_numerical,
    create_xml_shortanswer,
    wrap_in_xml_body,
)

logger = logging.getLogger("yaml_tasks")

# load the signature from the moodlexml_writer functions to check the given dicts on errors
_func_params = {}  # will be filled with the parameters of the functions in FUNC_MAPPING
FUNC_MAPPING = {
    "description": create_xml_description,
    "essay": create_xml_essay,
    "multichoice": create_xml_multichoice,
    "numerical": create_xml_numerical,
    "shortanswer": create_xml_shortanswer,
}


def _load_parameters_from_writer():
    for name, func in FUNC_MAPPING.items():
        _func_params[name] = {}
        for param in signature(func).parameters.values():
            _type_string = ""
            if "function" in str(type(param.annotation)) or "class" in str(param.annotation):
                _type_string = str(param.annotation.__name__)
            else:
                _type_string = str(param.annotation)
            _type_string = _type_string.replace("typing.", "")
            _func_params[name][param.name] = {
                "required": "inspect._empty" in str(param.default),
                "type": _type_string,
            }
            if not _func_params[name][param.name]["required"]:
                _func_params[name][param.name]["default"] = str(param.default)
        for param in parse(func.__doc__).params:
            if param.arg_name in _func_params[name]:
                _func_params[name][param.arg_name]["description"] = param.description


_load_parameters_from_writer()


def _set_docstring(function, func_name: str):
    """Dynamically adds docstrings to a function from FUNC_MAPPING."""
    string_stream = io.StringIO("")

    def pretty_print(_dict, i):
        for key, value in _dict.items():
            print(indent(str(key) + ":", "    " * i), file=string_stream)
            if isinstance(value, dict):
                pretty_print(value, i + 1)
            else:
                print(indent(str(value), "    " * (i + 1)), file=string_stream)

    pretty_print(_func_params[func_name], 3)
    _doc = f"""\
    Add a {func_name} question to a certain category.

    Args:
        category:
            The Moodle category where the question should be imported.
        arguments:
            The arguments for the creator function, the following keys are allowed/required:
            \n{indent(string_stream.getvalue(), "    ")}
    """
    function.__doc__ = dedent(_doc)


def _check_arg_dict(func_name: str, args: dict):
    """Checks if a dict of arguments for the function func_name matches it's signature."""
    _required = []
    _not_required = []
    for arg, properties in _func_params[func_name].items():
        if properties["required"]:
            _required.append(arg)
        else:
            _not_required.append(arg)
    for arg in _required:
        if arg not in args:
            raise ValueError(f"Argument {arg} is required to create a {func_name} question.")
    for arg in dict(args).keys():
        if arg not in _func_params[func_name]:
            logger.warning(
                "Argument %s is not supported for a %s question. The argument will be removed.",
                arg,
                func_name,
            )
            del args[arg]


class MoodleXmlBuilder(Builder):
    """Class representing a single MoodleXML document and functions to define it."""

    def __init__(self):
        self._xml = ""
        self._data = {}

        # add functions for each entry type for direct access
        self.add_description = partial(self.add_entry, "description")
        _set_docstring(self.add_description, "description")

        self.add_essay = partial(self.add_entry, "essay")
        _set_docstring(self.add_essay, "essay")

        self.add_multichoice = partial(self.add_entry, "multichoice")
        _set_docstring(self.add_multichoice, "multichoice")

        self.add_numerical = partial(self.add_entry, "numerical")
        _set_docstring(self.add_numerical, "numerical")

        self.add_shortanswer = partial(self.add_entry, "shortanswer")
        _set_docstring(self.add_shortanswer, "shortanswer")

    def _create_xml(self):
        """Creates an XML string in self._xml based on the data in self._data."""
        logger.info("Start creating XML")
        _xml_items = []
        for category, questions in self._data.items():
            _xml_items.append(create_xml_category(category))
            for question in questions:
                _func = FUNC_MAPPING.get(question["type"])
                if _func is None:
                    raise ValueError("types does not match any defined function")
                _xml_items.append(_func(**question["args"]))
        xml = "\n".join(_xml_items)
        self._xml = wrap_in_xml_body(xml)

    def add_entry(self, type_: str, category: str, arguments: dict) -> None:
        """Adds a single entry to the result."""
        _check_arg_dict(type_, arguments)
        _entry = {"type": type_, "args": arguments}
        if category not in self._data:
            self._data[category] = []
        self._data[category].append(_entry)

    def write_result(self, output_path: str) -> None:
        """Write the resulting file to output_path."""
        self._create_xml()
        with open(output_path, "w", encoding="utf-8") as file:
            file.write(self._xml)
        logger.info("Finish writing XML to %s", output_path)
