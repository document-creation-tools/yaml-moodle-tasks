"""Basic API to create XML strings according to Moodle's specification.
https://docs.moodle.org/30/en/Moodle_XML_format
"""
import base64
import logging
import os
from textwrap import dedent
from typing import Dict, List, Optional, Tuple, Union
from urllib.parse import urlparse

import requests
from bs4 import BeautifulSoup

from .customtypes import (
    AllowedFraction,
    Answer,
    Feedback,
    HtmlString,
    ResponseFieldHeight,
    ResponseFormat,
    XmlString,
)

logger = logging.getLogger("yaml_tasks")


def wrap_in_xml_body(content: XmlString) -> XmlString:
    """Add header and quiz tags around given XML."""
    res = f"""\
        <?xml version="1.0" encoding="UTF-8"?>
        <quiz>
            {_indent(content)}
        </quiz>
        """
    return XmlString(dedent(res))


def create_xml_category(category: str) -> XmlString:
    """Create/Start an Moodle XML category."""
    res = f"""\
        <!-- start category -->
        <question type="category">
            <category>
                <text>{category}</text>
            </category>
        </question>
        """
    return XmlString(dedent(res))


def create_xml_description(
    title: HtmlString,
    description: HtmlString,
    file_paths: List[str],
) -> XmlString:
    """Creates a Moodle XML entry for a description field."""
    res = f"""\
        <question type="description">
            {_indent(_create_xml_title(title))}
            {_indent(_create_xml_question_text(description, file_paths))}
            <generalfeedback format="html"><text></text></generalfeedback>
            <defaultgrade>0.0000000</defaultgrade>
            <penalty>0.0000000</penalty>
            <hidden>0</hidden>
        </question>
        """
    return XmlString(dedent(res))


def create_xml_multichoice(
    title: HtmlString,
    question: HtmlString,
    points: Union[int, float],
    answers: List[Answer],
    file_paths: List[str],
    single_choice: bool = False,
    feedback: Dict[Feedback, HtmlString] = None,
    answer_numbering: str = "none",
    shuffle_answers: bool = False,
) -> XmlString:
    """Creates a Moodle XML entry for a single- or multiple-choice question.

    Args:
        title:
            The question's title.
        question:
            The question text, its description.
        points:
            How much the question is worth (max. possible points).
        answers:
            Dict, where keys 'text' and 'fraction' are required and 'feedback' is optional.
            'text' is the answer's text, 'fraction' is the percentage of the points that is added/
            subtracted if the answer is chosen. If the percentage is positive, it is a correct
            answer, and negative for an incorrect one.
        file_paths:
            List of files that should be linked to in the question.
            The first element is always a prefixpath for all other files.
        single_choice:
            Whether one (True) or more (False) answers can be selected.
        feedback:
            Feedback texts for different cases.
        answer_numbering:
            How (if not 'none') the answers are numbered.
            Possible values: 'none', 'abc', 'ABCD' or '123'
        shuffle_answers:
            If Moodle should shuffle the answers.

    Returns:
        The question in Moodle XML format (string).
    """
    if feedback is None:
        feedback = {}
    generalfeedback = HtmlString(feedback["general"] if "general" in feedback else "")
    correctfeedback = HtmlString(feedback["correct"] if "correct" in feedback else "")
    partiallycorrectfeedback = HtmlString(
        feedback["partiallycorrect"] if "partiallycorrect" in feedback else ""
    )
    incorrectfeedback = HtmlString(feedback["incorrect"] if "incorrect" in feedback else "")
    _answers = XmlString(
        "\n".join([_create_xml_answer(**a, text_is_html=True) for a in answers])  # type: ignore
    )

    res = f"""\
        <question type="multichoice">
            {_indent(_create_xml_title(title))}
            {_indent(_create_xml_question_text(question, file_paths))}
            <defaultgrade>{points}</defaultgrade>
            <penalty>0.3333333</penalty>
            <hidden>0</hidden>
            <single>{str(single_choice).lower()}</single>
            <shuffleanswers>{str(shuffle_answers).lower()}</shuffleanswers>
            <answernumbering>{answer_numbering}</answernumbering>
            <generalfeedback format="html">
                <text>{_escape_html(generalfeedback)}</text>
            </generalfeedback>
            <correctfeedback format="html">
                <text>{_escape_html(correctfeedback)}</text>
            </correctfeedback>
            <partiallycorrectfeedback format="html">
                <text>{_escape_html(partiallycorrectfeedback)}</text>
            </partiallycorrectfeedback>
            <incorrectfeedback format="html">
                <text>{_escape_html(incorrectfeedback)}</text>
            </incorrectfeedback>
            <shownumcorrect/>
            {_indent(_answers)}
        </question>
        """
    return XmlString(dedent(res))


def create_xml_essay(
    title: HtmlString,
    question: HtmlString,
    points: Union[int, float],
    file_paths: List[str],
    responseformat: ResponseFormat = "editor",
    responserequired: bool = False,
    responsetemplate: HtmlString = None,
    responsefieldlines: ResponseFieldHeight = 15,
    attachments: int = 0,
    attachmentsrequired: int = 0,
    generalfeedback: HtmlString = None,
) -> XmlString:
    """Creates a Moodle XML entry for a essay question.

    Args:
        title:
            The question's title.
        question:
            The question text, its description.
        points:
            How much the question is worth (max. possible points).
        file_paths:
            List of files that should be linked to in the question.
            The first element is always a prefixpath for all other files.
        responseformat:
            Style of the textfield. Can be:
            'editor': HTML input
            'editorfilepicker': HTML input with included file picker
            'plain': Plain (Non-HTML) text input with non-monospace font.
            'monospaced': Plain (Non-HTML) text input with monospace font.
        responserequired:
            Determines whether the user has to enter text or not.
        responsetemplate:
            Text that filles the response field when the user starts the task.
            Supports Markdown, but if 'responseformat' is 'plain' or 'monospaced' the
            resulting HTML will not be rendered, but displayed as text.
        responsefieldlines:
            Hight in lines of the input field defined by 'responseformat'.
            Moodle frontend only allows: 5, 10, 15, 20, 25, 30, 35, 40
            TODO: Test what happens if number is not in the above set (especially 0
            as that might remove the textfield, which would be nice for file-only answers).
        attachments:
            Number of allowed files to be attached.
            Possible values:
            -1: Not limited
            0: No attachments allowed.
            1-3: One to three files are allowed.
            TODO: Test what happens if number is >= 4 (Moodle frontend only allows 3).
        attachmentsrequired:
            Number of required files to be attached.
            Possible values:
            0: Attaching files is optional.
            1-3: One to three files are required.
            TODO: Test what happens if number is >= 4 (Moodle frontend only allows 3).
        generalfeedback:
            Feedback text.
        TODO: determine if the XML format supports setting restrictions on the submitted files.

    Returns:
        The question in Moodle XML format (string).
    """
    _generalfeedback = _handle_optional_html_parameter(generalfeedback)
    _responsetemplate = _handle_optional_html_parameter(responsetemplate)

    res = f"""\
        <question type="essay">
            {_indent(_create_xml_title(title))}
            {_indent(_create_xml_question_text(question, file_paths))}
            <defaultgrade>{points}</defaultgrade>
            <penalty>0.0000000</penalty>
            <hidden>0</hidden>
            <responseformat>{responseformat}</responseformat>
            <responsetemplate format="html">
                <text>{_responsetemplate}</text>
            </responsetemplate>
            <responserequired>{int(responserequired)}</responserequired>
            <responsefieldlines>{responsefieldlines}</responsefieldlines>
            <attachments>{attachments}</attachments>
            <attachmentsrequired>{attachmentsrequired}</attachmentsrequired>
            <graderinfo format="html">
                <text></text>
            </graderinfo>
            <generalfeedback format="html">
                <text>{_generalfeedback}</text>
            </generalfeedback>
        </question>
        """
    return XmlString(dedent(res))


def create_xml_numerical(
    title: HtmlString,
    question: HtmlString,
    points: Union[int, float],
    answers: List[Answer],
    file_paths: List[str],
    generalfeedback: HtmlString = None,
) -> XmlString:
    """Creates a Moodle XML entry for a numerical question.
    Units are currently not supported.

    Args:
        title:
            The question's title.
        question:
            The question text, its description.
        points:
            How much the question is worth (max. possible points).
        answers:
            Dict, where keys 'text' and 'fraction' are required and 'feedback' and 'tolerance' are
            optional.
            'text' is the solution against which is compared.
            'fraction' is the percentage of the points that is rewarded if the answer is chosen.
            'tolerance' is the absolute number that the answer can be off the correct value.
        file_paths:
            List of files that should be linked to in the question.
            The first element is always a prefixpath for all other files.
        generalfeedback:
            Feedback text.

    Returns:
        The question in Moodle XML format (string).
    """
    _generalfeedback = _handle_optional_html_parameter(generalfeedback)
    _answers = XmlString(
        "\n".join([_create_xml_answer(**a, text_is_html=False) for a in answers])  # type: ignore
    )

    res = f"""\
        <question type="numerical">
            {_indent(_create_xml_title(title))}
            {_indent(_create_xml_question_text(question, file_paths))}
            <defaultgrade>{points}</defaultgrade>
            <penalty>0.3333333</penalty>
            <hidden>0</hidden>
            <generalfeedback format="html">
                <text>{_generalfeedback}</text>
            </generalfeedback>
            {_indent(_answers)}
        </question>
        """
    return XmlString(dedent(res))


def create_xml_shortanswer(
    title: HtmlString,
    question: HtmlString,
    points: Union[int, float],
    answers: List[Answer],
    file_paths: List[str],
    usecase: bool = True,
    generalfeedback: HtmlString = None,
) -> XmlString:
    """Creates a Moodle XML entry for a shortanswer question.
    Units are currently not supported.

    Args:
        title:
            The question's title.
        question:
            The question text, its description.
        points:
            How much the question is worth (max. possible points).
        answers:
            Dict, where keys 'text' and 'fraction' are required and 'feedback' and 'tolerance' are
            optional.
            'text' is the solution against which is compared.
            'fraction' is the percentage of the points that is rewarded if the answer is chosen.
        usecase:
            Determines if the answer is correct/incorrect if only the case does not fit the provided
            correct answer.
        file_paths:
            List of files that should be linked to in the question.
            The first element is always a prefixpath for all other files.
        generalfeedback:
            Feedback text.

    Returns:
        The question in Moodle XML format (string).
    """
    _generalfeedback = _handle_optional_html_parameter(generalfeedback)
    _answers = XmlString(
        "\n".join([_create_xml_answer(**a, text_is_html=False) for a in answers])  # type: ignore
    )

    res = f"""\
        <question type="shortanswer">
            {_indent(_create_xml_title(title))}
            {_indent(_create_xml_question_text(question, file_paths))}
            <defaultgrade>{points}</defaultgrade>
            <penalty>0.3333333</penalty>
            <hidden>0</hidden>
            <generalfeedback format="html">
                <text>{_generalfeedback}</text>
            </generalfeedback>
            <usecase>{int(usecase)}</usecase>
            {_indent(_answers)}
        </question>
        """
    return XmlString(dedent(res))


def _indent(text, depth=3):
    """Indentation of text for better looking XML output. Disabled for now as it caused problems
    with e.g. <per> tags in the text."""
    return text
    # lines = text.split("\n")
    # for i, line in enumerate(lines):
    #     if i == 0 or line.strip() == "":
    #         continue
    #     lines[i] = "    " * depth + line

    # return "\n".join(lines)


def _wrap_in_cdata(content: HtmlString) -> XmlString:
    return XmlString(f"<![CDATA[{content}]]>")


def _force_partial_html(html: HtmlString) -> HtmlString:
    return HtmlString(
        html.replace("<html>", "")
        .replace("<body>", "")
        .replace("</body>", "")
        .replace("</html>", "")
    )


def _escape_html(content: HtmlString) -> XmlString:
    return _wrap_in_cdata(_force_partial_html(content))


def _handle_optional_html_parameter(param: Union[HtmlString, Optional[HtmlString]]) -> XmlString:
    """The markdown module would create '<p></p>' for an empty-string input and
    '<p>None</p>' for a None-input. Hence these cases are handled here.
    """
    return _escape_html(param) if param else XmlString("")


def _create_xml_answer(
    text: HtmlString,
    text_is_html: bool,
    fraction: AllowedFraction,
    feedback: HtmlString = None,
    tolerance: Union[int, float] = None,
) -> XmlString:
    """Creates a Moodle XML answer.

    multichoice allows a HTML text, the numerical and short don't.
    Only numerical supports tolerance.
    """
    if text_is_html:
        _text = _escape_html(HtmlString(text))
        _format = "html"
    else:
        _text = _wrap_in_cdata(HtmlString(text))
        _format = "moodle_auto_format"
    _feedback = _handle_optional_html_parameter(feedback)
    if tolerance is not None:
        _tolerance = f"\n            <tolerance>{tolerance}</tolerance>"
    else:
        _tolerance = ""
    res = f"""\
        <answer fraction="{fraction}" format="{_format}">
            <text>{_indent(_text, depth=4)}</text>
            <feedback format="html">
                <text>{_indent(_feedback, depth=5)}</text>
            </feedback>{_tolerance}
        </answer>
        """
    return XmlString(dedent(res))


def _create_xml_title(title: str) -> XmlString:
    """Creates a Moodle XML name tag."""
    res = f"""\
        <name>
            <text>{title}</text>
        </name>
        """
    return XmlString(dedent(res))


def _create_xml_question_text(description: HtmlString, file_paths: List[str]) -> XmlString:
    """Creates the Moodle XML string for a questiontext.

    This function converts the Markdown string to HTML and inlines all '<img' tags as files
    in the Moodle XML format as Base64.
    All files referenced in the file_paths list will be inlined as Base64 and a HTML link
    '<a' is added as a reference at the end of the description.
    The first element in `file_paths` is always a prefixpath for all other files and images.
    """
    file_tags: List[XmlString] = []
    _description, _file_tags = _inline_images(description, file_paths[0])
    file_tags += _file_tags
    _description, _file_tags = _add_files(_description, file_paths)
    file_tags += _file_tags
    files = "\n".join(file_tags)
    _description_ = _wrap_in_cdata(_description)

    res = f"""\
        <questiontext format="html">
            <text>{_indent(_description_, depth=4)}</text>
            {_indent(files)}
        </questiontext>
        """
    return XmlString(dedent(res))


def _create_b64_filetag(file_name: str, file: bytes) -> XmlString:
    """Creates a Moodle XML file tag with the 'file' inlined as Base64.
    The file can be linked in a sibling '<text>' tag as '@@PLUGINFILE@@/{file_name}'.
    """
    logger.debug("Encode %s to Base64", file_name)
    b64_string = repr(base64.b64encode(file))[2:-1]
    return XmlString(f'<file name="{file_name}" path="/" encoding="base64">{b64_string}</file>')


def _inline_images(html: HtmlString, _path_prefix: str) -> Tuple[HtmlString, List[XmlString]]:
    """Replaces the `<img` tags withing the HTML to the Moodle XML way of inlining files
    The Base64-encoded images are included in the returned list as Moodle XML file tags.
    """
    image_file_tags = []
    soup = BeautifulSoup(html, "lxml")
    for img_tag in soup.find_all("img"):
        logger.info("Process image: %s", img_tag)
        if str(img_tag["src"])[:4] == "http":  # online file
            file_name = os.path.basename(urlparse(img_tag["src"]).path)
            try:
                res = requests.get(img_tag["src"])
                if res.status_code == 200:
                    image_file_tags.append(_create_b64_filetag(file_name, res.content))
                    img_tag["src"] = f"@@PLUGINFILE@@/{file_name}"
                    if img_tag.get("alt") == "":
                        img_tag["alt"] = file_name
                else:
                    logger.warning(
                        "Could not request %s, status: %s",
                        img_tag["src"],
                        res.status_code,
                    )
            except requests.exceptions.RequestException as exception:
                logger.warning("Error while requesting %s. %s", img_tag["src"], exception)
        else:  # local file
            try:
                file_name = os.path.basename(img_tag["src"])
                _path = os.path.abspath(os.path.join(_path_prefix, img_tag["src"]))
                logger.debug("Resulting absolute path %s", _path)
                with open(_path, "rb") as file:
                    image_file_tags.append(_create_b64_filetag(file_name, file.read()))
                if img_tag.get("alt") == "":
                    img_tag["alt"] = file_name
                img_tag["src"] = f"@@PLUGINFILE@@/{file_name}"
            except OSError as exception:
                logger.warning("Could not read file %s. %s", img_tag["src"], exception)
    return _force_partial_html(HtmlString(str(soup))), image_file_tags


def _add_files(html: HtmlString, file_paths: list) -> Tuple[HtmlString, List[XmlString]]:
    """Adds tags for appended files to the HTML string.
    The Base64-encoded files are included in the returned list as Moodle XML file tags.
    The first element in `file_paths` is always a prefixpath for all other files.
    """
    file_tags = []
    _path_prefix = file_paths[0]
    file_paths = file_paths[1:]
    for file_path in file_paths:
        logger.info("Process file: %s", file_path)
        file_path = os.path.abspath(os.path.join(_path_prefix, file_path))
        logger.debug("Resulting absolute path %s", file_path)
        try:
            with open(file_path, "rb") as file:
                file_name = os.path.basename(file_path)
                html = HtmlString(
                    html + f'\n<p><a title="{file_name}" href="@@PLUGINFILE@@/{file_name}"'
                    f'target="_blank">{file_name}</a><p/>'
                )
                file_tags.append(_create_b64_filetag(file_name, file.read()))
        except OSError as exception:
            logger.warning("Could not read file %s. %s", file_path, exception)
    return html, file_tags
