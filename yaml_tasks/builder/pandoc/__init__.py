"""Builder for pandoc files."""
import logging
import os
from textwrap import indent

import pypandoc
from bs4 import BeautifulSoup
from docstring_parser import parse

from .. import Builder

logger = logging.getLogger("yaml_tasks")


class PandocBuilder(Builder):
    """Class for the creation of documents through pandoc."""

    def __init__(
        self,
        output_format: str = "beamer",
        flags: str = "",
        hide_task_with_solution: bool = False,
        hide_task_wout_solution: bool = False,
    ):
        self.out_format = output_format
        self.flags = flags[1:-1].split(" ") if flags is not None else []
        self.hide_task_with_solution = hide_task_with_solution
        self.hide_task_wout_solution = hide_task_wout_solution
        self._html = ""
        self.marking = {
            "correct": "&#10004;",
            "partially_correct_mc": "&#10003;",
            "partially_correct_sc": "(&#10004;)",
            "wrong": "",
            "wrong_shortanswer": "&#10060;",
        }

    def _add_solution_marking(
        self, text, min_frac, max_frac, frac, which_partially_corr, which_wrong="wrong"
    ):
        """Marks an item according to their 'correctness' based on their fraction."""
        if frac == max_frac:
            text += f" {self.marking['correct']}"
        elif frac < max_frac and frac > min_frac:
            text += f" {self.marking[which_partially_corr]}"
        elif frac < min_frac:
            text += f" {self.marking[which_wrong]}"
        elif frac == min_frac:
            text = f"<del>{text}</del>"
        return text

    def _create_multichoice_entry(self, args: dict, show_solution: bool):
        res = ['<ol type="1">']
        if args["single_choice"]:
            for answer in args["answers"]:
                _text = answer["text"]
                if show_solution:
                    _text = self._add_solution_marking(
                        _text,
                        1,
                        100,
                        answer["fraction"],
                        "partially_correct_sc",
                    )
                res.append(f"<li><span>&nbsp;{_text}</span></li>")
        else:
            _max_frac = max(args["answers"], key=lambda x: x["fraction"])["fraction"]
            for answer in args["answers"]:
                _text = answer["text"]
                if show_solution:
                    _text = self._add_solution_marking(
                        _text,
                        0,
                        _max_frac,
                        answer["fraction"],
                        "partially_correct_mc",
                    )
                res.append(f"<li><span>&nbsp;{_text}</span></li>")
        return res + ["</ol>"]

    def _create_shortanswer_entry(self, args: dict, show_solution: bool):
        res = ["<p></p>"]
        if show_solution:
            for answer in args["answers"]:
                _text = str(answer["text"])
                _text = self._add_solution_marking(
                    _text,
                    0,
                    100,
                    answer["fraction"],
                    "partially_correct_sc",
                    which_wrong="wrong_shortanswer",
                )
                res.append(f"<p><span>&nbsp;{_text}</span></p>")
        return res

    def _create_nop_entry(self, *_):
        return []

    def add_entry(self, type_: str, _, arguments: dict) -> None:
        """Adds a single entry to the result."""

        def _handle_solutions(callback, has_solution):
            res = []
            if type_ == "description":
                _text = [arguments["description"]]
            else:
                _text = [arguments["question"]]
            if has_solution:
                if not self.hide_task_wout_solution:
                    res += _text + callback(arguments, False) + ["<hr />"]
                if not self.hide_task_with_solution:
                    res += _text + callback(arguments, True) + ["<hr />"]
            else:
                res += _text
                res.append("<hr />")
            return res

        _handler_mapping = {
            "multichoice": {
                "callback": self._create_multichoice_entry,
                "has_solution": True,
            },
            "shortanswer": {
                "callback": self._create_shortanswer_entry,
                "has_solution": True,
            },
            "essay": {
                "callback": self._create_nop_entry,
                "has_solution": False,
            },
            "description": {
                "callback": self._create_nop_entry,
                "has_solution": False,
            },
        }

        _res_string = "\n".join(_handle_solutions(**_handler_mapping[type_]))
        _res_string = self._make_image_paths_absolute(arguments["file_paths"][0], _res_string)
        self._html += _res_string

    def write_result(self, output_path: str) -> None:
        """Write the resulting file to output_path."""
        if self.out_format == "html":
            with open(output_path, "w", encoding="utf-8") as file:
                file.write(self._html)
        else:
            pypandoc.convert_text(
                self._html,  # input string
                self.out_format,  # format to convert to
                format="html+tex_math_single_backslash",  # format of the input string
                outputfile=output_path,  # file to write to
                extra_args=self.flags,  # list of flags
                sandbox=False,  # https://github.com/jgm/pandoc/issues/8128
            )

    def _make_image_paths_absolute(self, path_prefix, html_string):
        soup = BeautifulSoup(html_string, "lxml")
        for img_tag in soup.find_all("img"):
            _path = os.path.abspath(os.path.join(path_prefix, img_tag["src"]))
            img_tag["src"] = _path
        return str(soup).replace("<html><body>", "").replace("</body></html>", "")
