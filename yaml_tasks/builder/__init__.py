"""Defines an abstract builder class to inherit from."""
from abc import ABC, abstractmethod


class Builder(ABC):
    """Abstract builder class, that any builder should inherit from."""

    @abstractmethod
    def add_entry(self, type_: str, category: str, arguments: dict) -> None:
        """Adds a single entry to the result."""

    def add_entries(self, data: dict) -> None:
        """Adds parsed entries from YamlParser in bulk."""
        for _, questions in sorted(data.items()):
            for _, question_obj in sorted(questions.items()):
                self.add_entry(
                    question_obj["type"], question_obj["category"], question_obj["properties"]
                )

    @abstractmethod
    def write_result(self, output_path: str) -> None:
        """Write the resulting file to output_path."""
